package com.seop.cosmeticmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CosmeticRequest {
    private String cosmeticType;

    private String cosmeticName;

    private String owner;

}
