package com.seop.cosmeticmanager.service;

import com.seop.cosmeticmanager.entity.Manager;
import com.seop.cosmeticmanager.repository.CosmeticRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManagerService {
    private final CosmeticRepository cosmeticRepository;

    public void setManager(String cosmeticType, String cosmeticName, String owner) {
        Manager addData = new Manager();
        addData.setCosmeticType(cosmeticType);
        addData.setCosmeticName(cosmeticName);
        addData.setOwner(owner);

        cosmeticRepository.save(addData);

    }
}
