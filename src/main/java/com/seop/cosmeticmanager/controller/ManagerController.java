package com.seop.cosmeticmanager.controller;

import com.seop.cosmeticmanager.model.CosmeticRequest;
import com.seop.cosmeticmanager.service.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manager")
@RequiredArgsConstructor
public class ManagerController {
    private final ManagerService managerService;

    @PostMapping("/data")
    private String getData(@RequestBody CosmeticRequest request) {
        managerService.setManager(request.getCosmeticType(), request.getCosmeticName(), request.getOwner());
        return "성공";
    }
}
