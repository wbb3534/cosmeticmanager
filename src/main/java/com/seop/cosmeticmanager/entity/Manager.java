package com.seop.cosmeticmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class Manager {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, length = 10)
    private String cosmeticType;

    @Column(unique = true, length = 20)
    private String cosmeticName;

    @Column(unique = true, length = 20)
    private String owner;
}
