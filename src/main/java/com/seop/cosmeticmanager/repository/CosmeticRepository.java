package com.seop.cosmeticmanager.repository;

import com.seop.cosmeticmanager.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CosmeticRepository extends JpaRepository<Manager, Long> {
}
